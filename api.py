#!/usr/bin/env python3

import argparse
import sys
from pprint import pprint
import requests

PARSER = argparse.ArgumentParser(
        description='Interact with jsonplaceholder todos.')
PARSER.add_argument('-g', '--get', action='store_true', help='Get 200 todos')
PARSER.add_argument('-d', '--delete', type=int, help='The id of a todo to delete')
PARSER.add_argument('-a', '--add', help='The description of a new todo')
PARSER.add_argument('-j', '--json', action='store_true', help='Print json results')

if len(sys.argv)==1:
    PARSER.print_help(sys.stderr)
    sys.exit(1)

ARGS = PARSER.parse_args()

API_URL = 'https://jsonplaceholder.typicode.com/todos'

if ARGS.get:
    params = {'_start': '0', '_end': '200'}
    todos = requests.get(API_URL, params)
    if todos.status_code == 200:
        if ARGS.json:
            print('TODOS: ' + str(len(todos.json())))
            pprint(todos.json())
        else:
            print ('Success. Got 200 todos. Use -j flag to see results.')

if ARGS.add:
    new_todo = {'userId': 1,
            'title': ARGS.add,
            'completed': False
            }

    make_new = requests.post(API_URL, new_todo)
    if make_new.status_code == 201:
        if ARGS.json:
            pprint(make_new.json())
        else:
            print('Successfully added ' + '"' + ARGS.add +'"')

if ARGS.delete:
    delete = requests.delete(API_URL + '/' + str(ARGS.delete))
    if delete.status_code == 200:
        print("Successfully deleted todo " + str(ARGS.delete))
    if ARGS.json:
        print(delete.json())
