CREATE TABLE IF NOT EXISTS users (
    user_id INT AUTO_INCREMENT,
    user_first_name VARCHAR(64) NOT NULL,
    user_last_name VARCHAR(64) NOT NULL,
    user_email VARCHAR(64) NOT NULL,
    user_password VARCHAR(255) NOT NULL,
    PRIMARY_KEY (user_id)
);

CREATE TABLE IF NOT EXISTS categories (
    category_id INT AUTO_INCREMENT,
    category_name VARCHAR(64) NOT NULL,
    category_description VARCHAR(255),
    category_count INT DEFAULT 0,
    PRIMARY_KEY (category_id)
);

CREATE TABLE IF NOT EXISTS articles (
    article_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    category_id INT,
    article_publish_status BOOLEAN NOT NULL,
    article_publish_date DATETIME NOT NULL,
    article_title TEXT NOT 64NULL,
    article_content LONGTEXT NOT NULL,
    article_summary TEXT NOT NULL,
    article_comment_status BOOLEAN NOT NULL,
    article_comment_count INT DEFAULT 0,
    PRIMARY_KEY (article_id),
    FOREIGN_KEY (user_id) REFERENCES users(user_id),
    FOREIGN_KEY (category_id) REFERENCES categories(category_id)
);

CREATE TABLE IF NOT EXISTS comments (
    comment_id INT AUTO_INCREMENT,
    article_id INT NOT NULL,
    user_id INT,
    comment_author VARCHAR(64) NOT NULL,
    comment_author_email VARCHAR(64) NOT NULL,
    comment_date DATETIME NOT NULL,
    comment_content TEXT NOT NULL,
    comment_approved BOOLEAN NOT NULL,
    PRIMARY_KEY (comment_id),
    FOREIGN_KEY (user_id) REFERENCES users(user_id),
    FOREIGN_KEY (article_id) REFERENCES articles(article_id)
);
