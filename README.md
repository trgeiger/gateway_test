# Database Design
This repo includes an image, `article_tables.jpg`, which illustrates the tables I would design for articles on Suggest.com, as well as `tables.sql` which includes SQL statements for creating those tables. I built the tables with a few assumptions:

1. An article can only have 1 category (based on Suggest.com having 6 categories in the menu), and might have no category.
2. Comments only respond to the article, not to other comments.
3. The authors of articles correspond to users in the website/CMS
4. Authors sometimes make comments, and we would want to associate that with their user account. This is why the optional foreign key `author_id` is included in the comments table.
5. If an author makes a comment, their user_email and user_first/last_name fields are independent of their comment_email and comment_author fields for display in the comment. In other words, the comment email and name are for external display in the comments, and are optional for an author. They need not be the same as the info in the users table.
6. Suggest.com is the only app or site running in this database, and so we don't require table prefixes.
7. The user_password field would contain a salted hash, not the actual password.

While there could be many more fields for different functions in every table, like reply chains for comments, modified timestamps for articles, revision histories, etc., I tried to keep things simple and get the basic functionality I can see on Suggest.com.

# API Interaction
*Requirements*: `requests` library, `pipenv install requests` or `pip install requests`

For this problem and the algorithms problem, I created simple command line applications. The simply-named `api.py` will show you the available flags when run without arguments or with the `--help` flag. I separated the todo fetching, todo creation, and todo deletion into separate flags. On success, the program prints helpful messages. You can also optionally print out the JSON result of the operation by providing the `--json` flag.

# Algorithms
`perm.py` takes a single argument, `FILE`, which is a text file containing one input string per line. It prints the comma-separated, unique permutations for each line in the input file on a single line.

I formatted `perm.py` as a Python module, since both defined functions could theoretically be useful in another program.

The runtime is `O(n!)`.
