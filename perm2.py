#!/usr/bin/env python3
import argparse
import sys

PARSER = argparse.ArgumentParser(
        description='Print the permutations of input strings in ascending order')
PARSER.add_argument("FILE", help='A text file with one input string per line')

# If no argument is supplied, display the help message
if len(sys.argv) == 1:
    PARSER.print_help(sys.stderr)
    sys.exit(1)

ARGS = PARSER.parse_args()
def permutation(pstr, plist, start=0):
    '''Fill input list with permutations of input string

    Backtracing function which fills a list with all the
    permutations of an input string. Note: duplicates occur
    if input string contains repeated characters.

    :param str pstr: Input string for permutations
    :param list plist: The list where we store our permutations
    :param int start: Starting index for permutations--used in backtracing calls
    '''
    # Base case: if start value has reached final index,
    # then we're ready to attempt adding to the heap

    if start == len(pstr) - 1:
        plist.append(pstr)

    else:
        # Turn our string into a list so we can swap characters
        pstr = list(pstr)

        # Loop through every case for each character by recursively
        # calling with starting index moved to the right
        for i in range(start, len(pstr)):
            # Swap two characters to check new permutation cases.
            # Note: first loop will not cause any swap, instead
            # passing pstr recursively to check with new start index
            pstr[start], pstr[i] = pstr[i], pstr[start]
            permutation(''.join(pstr), plist, start+1)

            # Reset our swap after recursion so we can backtrack
            # and finish checking the external loop cases
            pstr[start], pstr[i] = pstr[i], pstr[start]

def print_perms(pstr):
    '''Print a comma-separated, ordered list of permutations of input string'''

    # If our input is blank, print an error message
    if not pstr:
        print('There is no string to process. Please provide valid input.', file=sys.stderr)

    # initializing our list for use in permutation()
    out_list = []

    # make sure we are using a string
    pstr = str(pstr)

    # calculate our permutations and add them to out_list
    permutation(str(pstr), out_list)

    # deduplicate our values using a set transformation
    out_list = list(set(out_list))

    # sort our values with Python's timsort
    out_list = sorted(out_list)

    # print our comma-separated, ordered values with no ending comma
    print(','.join(str(i) for i in out_list))


if __name__ == "__main__":
    with open(ARGS.FILE, "r") as txt:
        for line in txt:
            print_perms(line.strip())
